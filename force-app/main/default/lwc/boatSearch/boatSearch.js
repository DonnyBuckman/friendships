import { LightningElement, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';

export default class BoatSearch extends LightningElement {
  isLoading = false;

  @wire(MessageContext)
  messageContext;

  // Handles loading event
  handleLoading() {}

  // Handles done loading event
  handleDoneLoading() {}

  // Handles search boat event
  // This custom event comes from the form
  searchBoats(event) {}

  createNewBoat() {}
}