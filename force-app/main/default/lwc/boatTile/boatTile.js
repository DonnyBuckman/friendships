import { LightningElement, api, wire } from 'lwc';
// import { getRecord } from 'lightning/uiRecordAPI';
import BOAT_OBJECT from '@salesforce/schema/Boat__c';
import NAME_FIELD from '@salesforce/schema/Boat__c.Name';
import PICTURE_FIELD from '@salesforce/schema/Boat__c.Boat_Image__c'
import OWNER_FIELD from '@salesforce/schema/Boat__c.Contact__c';
import PRICE_FIELD from '@salesforce/schema/Boat__c.Price__c';
import LENGTH_FIELD from '@salesforce/schema/Boat__c.Length__c';
import BOAT_TYPE_FIELD from '@salesforce/schema/Boat__c.BoatType__r.Name'
// // import { subscribe, MessageContext } from 'lightning/messageService';
// // import BOATMC from '@salesforce/messageChannel/BoatMessageChannel__c';
const TILE_WRAPPER_SELECTED_CLASS = "tile-wrapper selected";
const TILE_WRAPPER_UNSELECTED_CLASS =  "tile-wrapper";

export default class BoatTile extends LightningElement {
  @api boat = BOAT_OBJECT;
  selectedBoatId;
  // fields = [NAME_FIELD, PICTURE_FIELD, OWNER_FIELD, PRICE_FIELD, LENGTH_FIELD, BOAT_TYPE_FIELD];

  // context = createMessageContext();
  // subscription = null;

  // @wire(getRecord, { selectedBoatId: '$selectedBoatId'})
  // wiredBoat({data, error}) {
  //   console.log('Execute logic each time a new value is provisioned');
  //   if (data) {
  //     console.log('The function');
  //     this.data = data;
  //     this.error = undefined;
  //   } else if (error) {
  //     this.error = error;
  //     this.data = undefined;
  //   }
  // }


  // Getter for dynamically setting the background image for the picture
  // get backgroundStyle() {
  //   return 'background-image:url(${this.property.boatPicture__c})';
  // }

  // Getter for dynamically setting the tile class based on whether the
  // current boat is selected
  // get tileClass() {
    
  //   return this.selectedBoatId ? TILE_WRAPPER_SELECTED_CLASS : TILE_WRAPPER_UNSELECTED_CLASS;
  // }

  // Fires event with the Id of the boat that has been selected
  selectBoat() {
    if (this.selectedBoatId = !this.selectedBoatId) {
      const selectBoat = new CustomEvent("selectBoat", {
        detail: this.selectedBoatId
      });
    }    
    this.dispatchEvent(selectBoat);
  }
  // subscribeBOATMC() {
  //   if (this.subscription) {
  //     return;
  //   }
  //   this.subscription = subscribe(this.context, BOATMC, (message) => {
  //     this.handleMessage(message);
  //   });
  // }

  // unsubscribeBOATMC() {
  //   unsubscribe(this.subscription);
  //   this.subscription = null;
  // }
}