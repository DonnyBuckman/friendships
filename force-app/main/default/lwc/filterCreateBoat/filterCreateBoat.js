import { LightningElement, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';

const DELAY = 350;

/**
 * Display a filter panel to search for Boat__c[].
 */

export default class FilterCreateBoat extends LightningElement {
  value = 'Sailboat';

  get options() {
    return [
      { label: 'Sailboat', value: 'Sailboat' },
      { label: 'Fishing Boat', value: 'Fishing Boat' },
      { label: 'Jet Ski', value: 'Jet Ski' },
      { label: 'High Performance', value: 'High Performance' },
      { label: 'Pleasure Boat', value: 'Pleasure Boat' },
      { label: 'Ski Boat', value: 'Ski Boat' },
      { label: 'Yacht', value: 'Yacht' },
      { label: 'House Boat', value: 'House Boat' }
    ];
  }

  handleChange(event) {
    this.value = event.detail.value;
  }
}